#include "peripheral_api.h"
#include "neorv32.h"

#define ADC_MUX_0_PIN (30) // /NON_RF_ADC_S0
#define ADC_MUX_1_PIN (31) // /NON_RF_ADC_S1

#define NON_RF_ADC_SPI (1) // ADC088S102
#define RF_ADC_SERIAL_0 (2) // ADS7028
#define RF_ADC_SERIAL_1 (3) // ADC088S102

void setup_adcs() {
    gpio_set_dir(ADC_MUX_0_PIN, 1);
    gpio_set_dir(ADC_MUX_1_PIN, 1);

    spi_setup(NON_RF_ADC_SPI, 2, 1, 1, 1);
    spi_setup(RF_ADC_SERIAL_0, 2, 0, 0, 0);
    spi_setup(RF_ADC_SERIAL_1, 2, 1, 1, 1);
}

unsigned ads7028_cycle(int chan) {
    static int prev_chan;
    uint8_t high;
    uint8_t low;
    
    if(chan != prev_chan) {
        spi_set_cs(RF_ADC_SERIAL_0, 0);
        spi_write(RF_ADC_SERIAL_0, 0x08);
        spi_write(RF_ADC_SERIAL_0, 0x11);
        spi_write(RF_ADC_SERIAL_0, chan);
        while(spi_busy(RF_ADC_SERIAL_0));
        spi_set_cs(RF_ADC_SERIAL_0, 1);

        spi_set_cs(RF_ADC_SERIAL_0, 0);
        spi_write(RF_ADC_SERIAL_0, 0x08);
        spi_write(RF_ADC_SERIAL_0, 0x11);
        spi_write(RF_ADC_SERIAL_0, chan);
        while(spi_busy(RF_ADC_SERIAL_0));
        spi_set_cs(RF_ADC_SERIAL_0, 1);
        
        prev_chan = chan;

        //neorv32_cpu_delay_ms(1);
    }

    // Clear rx fifo
    while(spi_rx_avail(RF_ADC_SERIAL_0)) {
        spi_read(RF_ADC_SERIAL_0);
    }
    
    spi_set_cs(RF_ADC_SERIAL_0, 0);
    spi_write(RF_ADC_SERIAL_0, 0x08);
    spi_write(RF_ADC_SERIAL_0, 0x11);
    spi_write(RF_ADC_SERIAL_0, chan);
    while(spi_busy(RF_ADC_SERIAL_0));
    spi_set_cs(RF_ADC_SERIAL_0, 1);

    high = spi_read(RF_ADC_SERIAL_0);
    low = spi_read(RF_ADC_SERIAL_0);

    return (high << 8) | (low & 0xF0);
}

unsigned adc088s102_cycle(int ic, int chan) {
    static int prev_chan[2];
    int bus;
    uint16_t data;

    if(ic == 0) {
        bus = RF_ADC_SERIAL_1;
    }
    else {
        bus = NON_RF_ADC_SPI;
    }
    
    if(chan != prev_chan[ic]) {
        spi_set_cs(bus, 0);
        spi_write(bus, (chan << 11));
        while(spi_busy(bus));
        spi_set_cs(bus, 1);

        prev_chan[ic] = chan;
    }

    // Clear rx fifo
    while(spi_rx_avail(bus)) {
        spi_read(bus);
    }
    
    spi_set_cs(bus, 0);
    spi_write(bus, (chan << 11));
    while(spi_busy(bus));
    spi_set_cs(bus, 1);

    data = spi_read(bus);
    return data << 4;;
}

/* Channels 0..7: RF ADS7028
            8..15: RF ADC088S102
            16..35: Non-RF ADC088S102 (+mux)

   Normalised to MSB in bit 15 */
unsigned read_adc(int chan) {
    if(chan < 8) {
        return ads7028_cycle(chan);
    }
    else if(chan < 16) {
        return adc088s102_cycle(0, chan & 7);
    }
    else {
        if(chan < 24) {
            gpio_write(ADC_MUX_0_PIN, 1);
            gpio_write(ADC_MUX_1_PIN, 0);
        }
        else if(chan < 32) {
            gpio_write(ADC_MUX_0_PIN, 0);
            gpio_write(ADC_MUX_1_PIN, 0);
        }
        else {
            gpio_write(ADC_MUX_0_PIN, 0);
            gpio_write(ADC_MUX_1_PIN, 1);
        }
            
        return adc088s102_cycle(1, chan & 7);
    }
}
