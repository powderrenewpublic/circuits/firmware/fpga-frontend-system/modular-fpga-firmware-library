#ifndef wb_peripherals_h
#define wb_peripherals_h

#include "neorv32.h"

typedef struct __attribute__((packed, aligned(16))) {
    uint32_t DIR;
    uint32_t DIN;
    uint32_t DOUT;
} gpio_32_wb_t;

#define WB_GPIO_BASE (0xA0000000)
#define WB_GPIO_COUNT (12)
#define WB_GPIO ((volatile gpio_32_wb_t*) (WB_GPIO_BASE))

#define WB_SPI_BASE (0xA0000100)
#define WB_SPI_COUNT (4)
#define WB_SPI ((volatile neorv32_spi_t*) (WB_SPI_BASE))

#define WB_I2C_BASE (0xA0000200)
#define WB_I2C_COUNT (7)
#define WB_I2C ((volatile neorv32_twi_t*) (WB_I2C_BASE))

#endif // wb_peripherals_h

