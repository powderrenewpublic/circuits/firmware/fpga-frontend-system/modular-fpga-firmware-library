#include <stdint.h>

#include "i2c.h"
#include "peripheral_api.h"
#include "wb_peripherals.h"

#define ADDR_WRITE 0x00
#define ADDR_READ 0x01

#define NUM_9544S 2
#define CHAN_ENABLE 0x04
#define CHAN_DISABLE 0x00
static unsigned char ctrlregs[ NUM_9544S ];
static unsigned char addr_9544[ NUM_9544S ] = {
    0xE8, 0xEA // assumes DEBUG_GPIO15 is low
};

static int i2c_mux_disable(int mux) {
    if(ctrlregs[mux] == CHAN_DISABLE)
        // channel already disabled -- don't touch the bus to avoid
        // disturbing transaction in progress
        return 0;
    if(i2c_write(GPIO_RAW_CHANNEL, addr_9544[mux] | ADDR_WRITE, 1, 0) == 0)
        // Mux didn't send ack bit
        return -1;
    if(i2c_write(GPIO_RAW_CHANNEL, CHAN_DISABLE, 0, 1) == 0)
        // Mux didn't send ack bit
        return -1;

    ctrlregs[mux] = CHAN_DISABLE;

    return 0;
}

static int i2c_mux_enable( int mux, int muxchan ) {
    if( ctrlregs[ mux ] == ( CHAN_ENABLE | muxchan ) )
        // channel already enabled -- don't touch the bus to avoid
        // disturbing transaction in progress
        return 0;

    if( i2c_write( GPIO_RAW_CHANNEL, addr_9544[ mux ] |
                   ADDR_WRITE, 1, 0 ) == 0 )
        // Mux didn't send ack bit
        return -1;
    
    if( i2c_write( GPIO_RAW_CHANNEL, CHAN_ENABLE | muxchan, 0, 1 ) == 0 )
        // Mux didn't send ack bit
        return -1;
    
    ctrlregs[ mux ] = CHAN_ENABLE | muxchan;

    return 0;
}

static int i2c_mux_select(int chan) {
    int active_mux = -1;
    int muxchan;

    if(chan < WB_I2C_COUNT) {
        if(chan != GPIO_RAW_CHANNEL) {
            return 0;
        }
    }
    else if(0x10 <= chan && chan < 0x10 + NUM_9544S*4) {
        // Pass
    }
    else {
        // Invalid channel
        return -1;
    }

    switch( chan & 0xFC ) {
        case 0x10: // MUX U401
            active_mux = 0;
            muxchan = chan & 0x03;
            break;

        case 0x14: // MUX U402
            active_mux = 1;
            muxchan = chan & 0x03;
            break;
    }

    // Disable inactive MUX channels
    for(int i = 0; i < NUM_9544S; i++) {
        if(i == active_mux) continue;

        if(i2c_mux_disable(i) < 0)
            return -1;
    }

    if(active_mux == -1) {
        return 0;
    }

    return i2c_mux_enable(active_mux, muxchan);
}

extern int i2c_mux_write( int chan, unsigned char val, int start, int stop ) {
    if(i2c_mux_select(chan) < 0) return -1;
    if(chan & 0xF0) chan = GPIO_RAW_CHANNEL;

    return i2c_write( chan, val, start, stop );
}

extern unsigned char i2c_mux_read( int chan, int ack, int stop ) {
    if(i2c_mux_select(chan) < 0) return -1;
    if(chan & 0xF0) chan = GPIO_RAW_CHANNEL;

    return i2c_read( chan, ack, stop );
}

#define REG_INPUT 0x00
#define REG_OUTPUT 0x01
#define REG_CONFIG 0x03

#define NUM_GPIO_BANKS 4

static unsigned char gpio_addr[ NUM_GPIO_BANKS ] = {
    0x40, 0x48, 0x44, 0x4C
};

#define NUM_PG_BANKS 3

static unsigned char pg_addr[ NUM_PG_BANKS ] = {
    0x40, 0x48, 0x44
};

#define NUM_EN_BANKS 3

static unsigned char en_addr[ NUM_EN_BANKS ] = {
    0x4C, 0x42, 0x44
};

extern int i2c_9534_set_direction_single( int chan, int addr,
                                        unsigned char dir ) {

    if( !i2c_mux_write( chan, addr | ADDR_WRITE, 1, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, REG_CONFIG, 0, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, ~dir, 0, 1 ) )
        return -1;

    return 0;
}

extern unsigned char i2c_9534_in_single( int chan, int addr ) {
    
    if( !i2c_mux_write( chan, addr | ADDR_WRITE, 1, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, REG_INPUT, 0, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, addr | ADDR_READ, 1, 0 ) )
        return -1;
    
    return i2c_mux_read( chan, 0, 1 );
}

extern int i2c_9534_out_single( int chan, int addr, unsigned char val ) {

    if( !i2c_mux_write( chan, addr | ADDR_WRITE, 1, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, REG_OUTPUT, 0, 0 ) )
        return -1;
    
    if( !i2c_mux_write( chan, val, 0, 1 ) )
        return -1;

    return 0;
}

extern int i2c_gpio_set_direction( unsigned int dir ) {

    int i;

    for( i = 0; i < NUM_GPIO_BANKS; i++, dir >>= 8 )
        if( i2c_9534_set_direction_single( I2C_4_CHANNEL, gpio_addr[ i ],
                                           dir & 0xFF ) )
            return -1;

    return 0;
}

extern unsigned int i2c_gpio_in( void ) {
    
    int i;
    unsigned val = 0;
    
    for( i = 0; i < NUM_GPIO_BANKS; i++, val >>= 8 )
        val |= i2c_9534_in_single( I2C_4_CHANNEL, gpio_addr[ i ] ) << 24;

    return val;
}

extern int i2c_gpio_out( unsigned int val ) {

    int i;

    for( i = 0; i < NUM_GPIO_BANKS; i++, val >>= 8 )
        if( i2c_9534_out_single( I2C_4_CHANNEL, gpio_addr[ i ], val & 0xFF ) )
            return -1;

    return 0;
}

static unsigned char shared_bank_dir, shared_bank_out;

extern int i2c_pg_set_direction( unsigned int dir ) {

    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, pg_addr[ 0 ],
                                       dir & 0xFF ) )
        return -1;
    
    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, pg_addr[ 1 ],
                                     ( dir >> 8 ) & 0xFF ) )
        return -1;

    shared_bank_dir = ( shared_bank_dir & 0xFC ) | ( ( dir >> 16 ) & 0x03 );
    
    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, pg_addr[ 2 ],
                                       shared_bank_dir ) )
        return -1;

    return 0;
}

extern unsigned int i2c_pg_in( void  ) {

    return i2c_9534_in_single( I2C_3_CHANNEL, pg_addr[ 0 ] ) |
        ( i2c_9534_in_single( I2C_3_CHANNEL, pg_addr[ 1 ] << 8 ) ) |
        ( ( i2c_9534_in_single( I2C_3_CHANNEL, pg_addr[ 1 ] & 0x03 ) << 16 ) );
}

extern int i2c_pg_out( unsigned int val ) {
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, pg_addr[ 0 ], val & 0xFF ) )
        return -1;
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, pg_addr[ 1 ],
                             ( val >> 8 ) & 0xFF ) )
        return -1;

    shared_bank_out = ( shared_bank_out & 0xFC ) | ( ( val >> 16 ) & 0x03 );
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, pg_addr[ 2 ], shared_bank_out ) )
        return -1;

    return 0;
}

extern int i2c_en_set_direction( unsigned int dir ) {

    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, en_addr[ 0 ],
                                     dir & 0xFF ) )
        return -1;
    
    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, en_addr[ 1 ],
                                     ( dir >> 8 ) & 0xFF ) )
        return -1;

    shared_bank_dir = ( shared_bank_dir & 0xCF ) | ( ( dir >> 12 ) & 0x30 );
    
    if( i2c_9534_set_direction_single( I2C_3_CHANNEL, en_addr[ 2 ],
                                     shared_bank_dir ) )
        return -1;

    return 0;
}

extern unsigned int i2c_en_in( void  ) {
    
    return i2c_9534_in_single( I2C_3_CHANNEL, en_addr[ 0 ] ) |
        ( i2c_9534_in_single( I2C_3_CHANNEL, en_addr[ 1 ] << 8 ) ) |
        ( ( i2c_9534_in_single( I2C_3_CHANNEL, en_addr[ 1 ] & 0x30 ) << 12 ) );
}

extern int i2c_en_out( unsigned int val ) {
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, en_addr[ 0 ], val & 0xFF ) )
        return -1;
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, en_addr[ 1 ],
                             ( val >> 8 ) & 0xFF ) )
        return -1;

    shared_bank_out = ( shared_bank_out & 0xCF ) | ( ( val >> 12 ) & 0x30 );
    
    if( i2c_9534_out_single( I2C_3_CHANNEL, en_addr[ 2 ], shared_bank_out ) )
        return -1;

    return 0;
}

extern unsigned char i2c_ee_read_gen( unsigned addr, int channel, uint8_t i2c_addr ) {
    if( !i2c_mux_write( channel, i2c_addr | ADDR_WRITE, 1, 0 ) )
        return -1;
    
    if( !i2c_mux_write( channel, addr, 0, 0 ) )
        return -1;
    
    if( !i2c_mux_write( channel, i2c_addr | ADDR_READ, 1, 0 ) )
        return -1;
    
    return i2c_mux_read( channel, 0, 1 );
}

extern unsigned char i2c_ee_read( unsigned addr ) {
    return i2c_ee_read_gen(addr, MAC_CHANNEL, 0xA0);
}

extern int i2c_ee_write_gen( unsigned addr, unsigned char val, int channel, uint8_t i2c_addr) {
    if( !i2c_mux_write( channel, i2c_addr | ADDR_WRITE, 1, 0 ) )
        return -1;

    if( !i2c_mux_write( channel, addr, 0, 0 ) )
        return -1;

    if( !i2c_mux_write( channel, val, 0, 1 ) )
        return -1;

    return 0;
}

extern int i2c_ee_write( unsigned addr, unsigned char val ) {
    return i2c_ee_write_gen(addr, val, MAC_CHANNEL, 0xA0);
}

extern int i2c_ee_poll( void ) {
    
    int ret = i2c_mux_write( MAC_CHANNEL, 0xA0 | ADDR_WRITE, 1, 1 ) ? 0 : -1;

    return ret;
}

extern int i2c_ee_read_eui48( unsigned char *buf ) {

    int i;

    for( i = 0; i < 6; i++ )
        *buf++ = i2c_ee_read( 0xFA + i );

    return 0;
}
