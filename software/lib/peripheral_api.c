#include "peripheral_api.h"
#include "wb_peripherals.h"
#include "neorv32.h"

void gpio_set_dir(int gpio, int dir) {
    int gpio_bit = gpio & 0x1F;
    int gpio_handle_idx = gpio >> 5;

    if(dir) {
        WB_GPIO[gpio_handle_idx].DIR |= (1 << gpio_bit);
    }
    else {
        WB_GPIO[gpio_handle_idx].DIR &= ~(1 << gpio_bit);
    }
}

void gpio_write(int gpio, int val) {
    int gpio_bit = gpio & 0x1F;
    int gpio_handle_idx = gpio >> 5;

    if(val) {
        WB_GPIO[gpio_handle_idx].DOUT |= (1 << gpio_bit);
    }
    else {
        WB_GPIO[gpio_handle_idx].DOUT &= ~(1 << gpio_bit);
    }
}

int gpio_read(int gpio) {
    int gpio_bit = gpio & 0x1F;
    int gpio_handle_idx = gpio >> 5;

    return (WB_GPIO[gpio_handle_idx].DIN >> gpio_bit) & 1;
}

void i2c_setup(int channel, int prescaler) {
    WB_I2C[channel].CTRL = (1 << TWI_CTRL_EN) | (prescaler << TWI_CTRL_PRSC0);
}

int i2c_write(int channel, uint8_t byte, int send_start, int send_stop) {
    if(send_start) {
        WB_I2C[channel].CTRL |= (1 << TWI_CTRL_START);
        while(WB_I2C[channel].CTRL & (1 << TWI_CTRL_BUSY));
    }

    WB_I2C[channel].DATA = byte;
    while(WB_I2C[channel].CTRL & (1 << TWI_CTRL_BUSY));

    int ack = (WB_I2C[channel].CTRL >> TWI_CTRL_ACK) & 1;

    if(send_stop) {
        WB_I2C[channel].CTRL |= (1 << TWI_CTRL_STOP);
        while(WB_I2C[channel].CTRL & (1 << TWI_CTRL_BUSY));
    }

    return ack;
}

uint8_t i2c_read(int channel, int send_ack, int send_stop) {
    if(send_ack) {
        WB_I2C[channel].CTRL |= (1 << TWI_CTRL_MACK);
    }

    WB_I2C[channel].DATA = 0xFF;
    while(WB_I2C[channel].CTRL & (1 << TWI_CTRL_BUSY));

    uint8_t data = WB_I2C[channel].DATA;

    if(send_ack) {
        WB_I2C[channel].CTRL &= ~(1 << TWI_CTRL_MACK);
    }

    if(send_stop) {
        WB_I2C[channel].CTRL |= (1 << TWI_CTRL_STOP);
        while(WB_I2C[channel].CTRL & (1 << TWI_CTRL_BUSY));
    }

    return data;
}

void spi_setup(int channel, int prescaler, int cpol, int cpha, int word_size) {
    WB_SPI[channel].CTRL = (1 << SPI_CTRL_EN)
        | (prescaler << SPI_CTRL_PRSC0)
        | (cpol << SPI_CTRL_CPOL)
        | (cpha << SPI_CTRL_CPHA)
        | (word_size << SPI_CTRL_SIZE0);
}

void spi_set_cs(int channel, int cs) {
    if(cs) {
        WB_SPI[channel].CTRL &= ~(1 << SPI_CTRL_CS0);
    }
    else {
        WB_SPI[channel].CTRL |= (1 << SPI_CTRL_CS0);
    }
}

void spi_write(int channel, uint32_t data) {
    WB_SPI[channel].DATA = data;
}

uint32_t spi_read(int channel) {
    return WB_SPI[channel].DATA;
}

int spi_rx_avail(int channel) {
    return (WB_SPI[channel].CTRL >> SPI_CTRL_RX_AVAIL) & 1;
}

int spi_tx_half_full(int channel) {
    return (WB_SPI[channel].CTRL >> SPI_CTRL_TX_HALF) & 1;
}

int spi_busy(int channel) {
    return (WB_SPI[channel].CTRL >> SPI_CTRL_BUSY) & 1;
}
