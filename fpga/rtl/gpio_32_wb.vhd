library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity gpio_32_wb is
    port (
        gpio_dir_o : out std_logic_vector(31 downto 0);
        gpio_o : out std_logic_vector(31 downto 0);
        gpio_i : in std_logic_vector(31 downto 0);

        rst_i : in std_logic;
        clk_i : in std_logic;
        adr_i : in std_logic_vector(3 downto 0);
        dat_i : in std_logic_vector(31 downto 0);
        dat_o : out std_logic_vector(31 downto 0);
        we_i : in std_logic;
        stb_i : in std_logic;
        ack_o : out std_logic;
        cyc_i : in std_logic
    );
end gpio_32_wb;

architecture a1 of gpio_32_wb is
    signal dir : std_logic_vector(31 downto 0) := (others => '0');
    signal din : std_logic_vector(31 downto 0) := (others => '0');
    signal dout : std_logic_vector(31 downto 0) := (others => '0');

    signal stb_sync : std_logic := '0';
begin
    gpio_dir_o <= dir;
    gpio_o <= dout;

    ack_o <= stb_sync and cyc_i and stb_i and not rst_i;

    control : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                stb_sync <= '0';
            else
                stb_sync <= stb_i;
            end if;
        end if;
    end process;

    write : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                dir <= (others => '0');
                dout <= (others => '0');
            elsif cyc_i = '1' and stb_i = '1' and we_i = '1' then
        		case adr_i is
        			when "0000" => dir <= dat_i;
        			when "1000" => dout <= dat_i;
        			when others => NULL;
        		end case;
        	end if;
        end if;

    end process;

    read : process(clk_i)
    begin
        if rising_edge(clk_i) then
        	case adr_i is
        		when "0000" => dat_o <= dir;
        		when "0100" => dat_o <= din;
        		when "1000" => dat_o <= dout;
        		when others => dat_o <= (others => '0');
        	end case;
        end if;
    end process;

    gpio_input : process(clk_i)
        variable din_meta : std_logic_vector(31 downto 0) := (others => '0');
    begin
        if rising_edge(clk_i) then
        	din <= din_meta;

        	for i in 31 downto 0 loop
        		if dir(i) = '1' then
        			din_meta(i) := dout(i);
        		else
        			din_meta(i) := gpio_i(i);
        		end if;
        	end loop;
        end if;
    end process;
end architecture;
