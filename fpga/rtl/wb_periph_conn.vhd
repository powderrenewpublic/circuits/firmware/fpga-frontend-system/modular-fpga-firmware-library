library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.fpga_main_package.all;

entity wb_periph_conn is
    port (
        wb_rst : in std_logic;
        wb_clk : in std_logic;
        wb_adr : in std_logic_vector(31 downto 0);
        wb_we : in std_logic;
        wb_stb : in std_logic;
        wb_cyc : in std_logic;
        wb_dat_from_master : in std_logic_vector(31 downto 0);
        wb_dat_to_master : out std_logic_vector(31 downto 0);
        wb_ack : out std_logic;

		gpio_dir_o : out std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);
        gpio_o : out std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);
        gpio_i : in std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);

        spi_sck : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
        spi_sdo : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
        spi_sdi : in std_logic_vector(NUM_SPI_CORES-1 downto 0) := (others => 'U');
        spi_csn : out std_logic_vector(NUM_SPI_CORES-1 downto 0);

        i2c_sda : inout std_logic_vector(NUM_I2C_CORES-1 downto 0);
        i2c_scl : inout std_logic_vector(NUM_I2C_CORES-1 downto 0)
    );
end wb_periph_conn;

architecture a1 of wb_periph_conn is
    constant GPIO_BASE : std_logic_vector(31 downto 0) := x"A0000000";
    constant SPI_BASE : std_logic_vector(31 downto 0) := x"A0000100";
    constant I2C_BASE : std_logic_vector(31 downto 0) := x"A0000200";

    type gpio_dat_arr_t is array(NUM_GPIO_CORES-1 downto 0) of std_logic_vector(31 downto 0);
    type spi_dat_arr_t is array(NUM_SPI_CORES-1 downto 0) of std_logic_vector(31 downto 0);
    type i2c_dat_arr_t is array(NUM_I2C_CORES-1 downto 0) of std_logic_vector(31 downto 0);

    signal gpio_stb : std_logic_vector(NUM_GPIO_CORES-1 downto 0);
    signal gpio_ack : std_logic_vector(NUM_GPIO_CORES-1 downto 0);
    signal gpio_dat_arr : gpio_dat_arr_t;

    signal spi_stb : std_logic_vector(NUM_SPI_CORES-1 downto 0);
    signal spi_ack : std_logic_vector(NUM_SPI_CORES-1 downto 0);
    signal spi_dat_arr : spi_dat_arr_t;
    signal dummy_csn : std_logic_vector(7 downto 1);

    signal i2c_stb : std_logic_vector(NUM_I2C_CORES-1 downto 0);
    signal i2c_ack : std_logic_vector(NUM_I2C_CORES-1 downto 0);
    signal i2c_dat_arr : i2c_dat_arr_t;

    signal clkgen : std_logic_vector(7 downto 0);
begin
    control : process(gpio_ack, spi_ack, i2c_ack, wb_adr, wb_stb, gpio_dat_arr, spi_dat_arr, i2c_dat_arr)
    begin
        if gpio_ack = (gpio_ack'range => '0') and spi_ack = (spi_ack'range => '0') and i2c_ack = (i2c_ack'range => '0') then
            wb_ack <= '0';
        else
            wb_ack <= '1';
        end if;

        wb_dat_to_master <= (others => '0');
        for i in 0 to NUM_GPIO_CORES-1 loop
            if wb_adr(31 downto 4) = (GPIO_BASE(31 downto 4) or std_logic_vector(to_unsigned(i, 28))) then
                wb_dat_to_master <= gpio_dat_arr(i);
                gpio_stb(i) <= wb_stb;
            else
                gpio_stb(i) <= '0';
            end if;
        end loop;

        for i in 0 to NUM_SPI_CORES-1 loop
            if wb_adr(31 downto 3) = (SPI_BASE(31 downto 3) or std_logic_vector(to_unsigned(i, 29))) then
                wb_dat_to_master <= spi_dat_arr(i);
                spi_stb(i) <= wb_stb;
            else
                spi_stb(i) <= '0';
            end if;
        end loop;

        for i in 0 to NUM_I2C_CORES-1 loop
            if wb_adr(31 downto 3) = (I2C_BASE(31 downto 3) or std_logic_vector(to_unsigned(i, 29))) then
                wb_dat_to_master <= i2c_dat_arr(i);
                i2c_stb(i) <= wb_stb;
            else
                i2c_stb(i) <= '0';
            end if;
        end loop;
    end process;

    GPIO_INST:
    for i in 0 to NUM_GPIO_CORES-1 generate
        gpio_32_wb_inst_x : entity work.gpio_32_wb
        port map (
		    gpio_dir_o => gpio_dir_o(i*32 + 31 downto i*32),
            gpio_o => gpio_o(i*32 + 31 downto i*32),
            gpio_i => gpio_i(i*32 + 31 downto i*32),

            rst_i => wb_rst,
            clk_i => wb_clk,
            adr_i => wb_adr(3 downto 0),
            dat_i => wb_dat_from_master,
            dat_o => gpio_dat_arr(i),
            we_i => wb_we,
            stb_i => gpio_stb(i),
            ack_o => gpio_ack(i),
            cyc_i => wb_cyc
        );
    end generate;

    SPI_INST:
    for i in 0 to NUM_SPI_CORES-1 generate
        wb_spi_wrapper_inst_x : entity work.wb_spi_wrapper
        port map (
            clk => wb_clk,
            rst => wb_rst,

            wb_adr => wb_adr(2 downto 2),
            wb_we => wb_we,
            wb_stb => spi_stb(i),
            wb_cyc => wb_cyc,
            wb_dat_from_master => wb_dat_from_master,
            wb_dat_to_master => spi_dat_arr(i),
            wb_ack => spi_ack(i),

            clkgen_i => clkgen,

            spi_sck_o => spi_sck(i),
            spi_sdo_o => spi_sdo(i),
            spi_sdi_i => spi_sdi(i),
            spi_csn_o(0) => spi_csn(i),
            spi_csn_o(7 downto 1) => dummy_csn,

            irq_o => open
        );
    end generate;

    I2C_INST:
    for i in 0 to NUM_I2C_CORES-1 generate
        wb_i2c_wrapper_inst_x : entity work.wb_i2c_wrapper
        port map (
            clk => wb_clk,
            rst => wb_rst,

            wb_adr => wb_adr(2 downto 2),
            wb_we => wb_we,
            wb_stb => i2c_stb(i),
            wb_cyc => wb_cyc,
            wb_dat_from_master => wb_dat_from_master,
            wb_dat_to_master => i2c_dat_arr(i),
            wb_ack => i2c_ack(i),

            clkgen_i => clkgen,

            i2c_sda => i2c_sda(i),
            i2c_scl => i2c_scl(i),

            irq_o => open
        );
    end generate;

    clkgen_proc : process(wb_clk)
        variable clkdiv : std_logic_vector(7 downto 0) := (others => '0');
        variable prev_clkdiv : std_logic_vector(7 downto 0) := (others => '0');
    begin
        if rising_edge(wb_clk) then
            if wb_rst = '1' then
                clkdiv := (others => '0');
                prev_clkdiv := (others => '0');
            else
                for i in clkgen'range loop
                    clkgen(i) <= clkdiv(i) and not prev_clkdiv(i);
                end loop;

                prev_clkdiv := clkdiv;
                clkdiv := std_logic_vector(unsigned(clkdiv) + 1);
            end if;
        end if;
    end process;
end architecture;
