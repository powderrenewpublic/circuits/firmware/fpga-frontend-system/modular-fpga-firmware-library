library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity wb_read_reg is
    port (
        reg_val : in std_logic_vector(31 downto 0);
        reg_strobe : out std_logic;

        rst_i : in std_logic;
        clk_i : in std_logic;
        dat_o : out std_logic_vector(31 downto 0);
        we_i : in std_logic;
        stb_i : in std_logic;
        ack_o : out std_logic;
        cyc_i : in std_logic
    );
end wb_read_reg;

architecture a1 of wb_read_reg is
    signal prev_stb : std_logic := '0';
begin
    ack_o <= prev_stb and cyc_i and stb_i and not rst_i;
    dat_o <= reg_val;

    control: process(clk_i)
    begin
        if rising_edge(clk_i) then
            reg_strobe <= '0';
            prev_stb <= stb_i;

            if rst_i = '1' then
                prev_stb <= '0';
            elsif (not prev_stb and stb_i and cyc_i and not we_i) = '1' then
                reg_strobe <= '1';
            end if;
        end if;
    end process;
end architecture;
