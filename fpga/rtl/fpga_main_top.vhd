library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package fpga_main_top_package is
    -- PS_B_t describes the pins on the Conn_Power_B symbol
    -- missing pins are connected through an I2C MUX/GPIO IC
    type PS_B_t is record
        GPIO : std_logic_vector(11 downto 0);
        GPIO_C_EN : std_logic;
        GPIO_C_OSC : std_logic;
        SCL_1 : std_logic;
        SDA_1 : std_logic;
    end record;
end package;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.fpga_main_top_package.all;

entity fpga_main_top is
    port (
        PLL_CLK_IN : in std_logic;
        PLL_CLK_OUT : out std_logic;

        PLL_CE : out std_logic;
        PLL_SCLK : out std_logic;
        PLL_DATA : out std_logic;
        PLL_LE : out std_logic;
        PLL_MUXOUT : in std_logic;

        DCDC_OSC : out std_logic;

        -- Bits map to pins on RF_IO connector symbol in order starting from
        -- IO_3_3.0_N -> RF_IO_XX(0) to IO_2_5.7_P -> RF_IO_XX(19) 
        RF_IO_A0 : inout std_logic_vector(19 downto 0);
        RF_IO_A1 : inout std_logic_vector(19 downto 0);
        RF_IO_B0 : inout std_logic_vector(19 downto 0);
        RF_IO_B1 : inout std_logic_vector(19 downto 0);
        RF_IO_C0 : inout std_logic_vector(19 downto 0);
        RF_IO_C1 : inout std_logic_vector(19 downto 0);
        RF_IO_D0 : inout std_logic_vector(19 downto 0);
        RF_IO_D1 : inout std_logic_vector(19 downto 0);

        -- Sync signals for power A cards
        -- PS_A_SYNC(1 downto 0) -> PS_A_0_SYNC(1 downto 0)
        -- PS_A_SYNC(3 downto 2) -> PS_A_1_SYNC(1 downto 0)
        -- PS_A_SYNC(5 downto 4) -> PS_A_2_SYNC(1 downto 0)
        PS_A_SYNC : out std_logic_vector(5 downto 0);
        -- TODO split into 3 vectors

        -- See PS_B_t definition
        PS_B_A : inout PS_B_t;
        PS_B_B : inout PS_B_t;
        PS_B_C : inout PS_B_t;

        -- Bits map to IO signals on BOTTOM_IO connector as they appear in pin number order
        BOTTOM_IO_0 : inout std_logic_vector(19 downto 0);
        BOTTOM_IO_1 : inout std_logic_vector(19 downto 0);

        -- Bits map to NET names (DEBUG_GPIOXX)
        DEBUG_IO : inout std_logic_vector(13 downto 0);

        -- Bits map to IO signals on RF_MISC_IO connector as they appear in pin number order
        RF_MISC_IO : inout std_logic_vector(5 downto 0);

        SD_CLK : out std_logic;
        SD_CMD : inout std_logic;
        SD_DAT : inout std_logic_vector(3 downto 0);
        SD_DET : in std_logic_vector(1 downto 0); -- 0 -> DETA, 1 -> DETB

        -- Bits map to NET names (MEM_GPIOXX)
        MEM_GPIO : inout std_logic_vector(43 downto 0);

        -- Bits map to NET names (CLK_PPS.GPIOX)
        CLK_PPS_GPIO : inout std_logic_vector(9 downto 0);
        CLK_PPS_PPS : out std_logic;
        CLK_PPS_10MHz : out std_logic;

        RF_ADC_0_CS : out std_logic;
        RF_ADC_0_SCLK : out std_logic;
        RF_ADC_0_MOSI : out std_logic;
        RF_ADC_0_MISO : in std_logic;

        RF_ADC_1_CS : out std_logic;
        RF_ADC_1_SCLK : out std_logic;
        RF_ADC_1_MOSI : out std_logic;
        RF_ADC_1_MISO : in std_logic;

        NON_RF_ADC_CS : out std_logic;
        NON_RF_ADC_SCLK : out std_logic;
        NON_RF_ADC_MOSI : out std_logic;
        NON_RF_ADC_MISO : in std_logic;
        NON_RF_ADC_MUX : out std_logic_vector(1 downto 0);

        EEPROM_SCL : inout std_logic;
        EEPROM_SDA : inout std_logic;

        I2C_MUX_SCL : inout std_logic;
        I2C_MUX_SDA : inout std_logic
    );
end fpga_main_top;
